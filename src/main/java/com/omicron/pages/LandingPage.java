package com.omicron.pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class LandingPage extends BasePageObject {

    private By appWithTabsButtonLocator = By.xpath("//*[@text='Demo App with Tabs']");

    public LandingPage(AndroidDriver<MobileElement> driver) {
        super(driver);
    }

    /**
     * Open `App with Tabs` page
     */
    public TabsAppGeneralPage navigateToAppWithTabsPage() {
        click(appWithTabsButtonLocator);
        return new TabsAppGeneralPage(driver);
    }
}
