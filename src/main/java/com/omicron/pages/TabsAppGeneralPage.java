package com.omicron.pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

public class TabsAppGeneralPage extends BasePageObject {

    private By quickTestButtonLocator = By.xpath("//*[@content-desc='Quick Test']");
    private By settingsButtonLocator = By.xpath("//*[@content-desc='Settings']");

    public TabsAppGeneralPage(AndroidDriver<MobileElement> driver) {
        super(driver);
    }

    /**
     * Navigate to Quick Test tab
     */
    public TabsAppQuickTestPage navigateToQuickTestPage() {
        click(quickTestButtonLocator);
        return new TabsAppQuickTestPage(driver);
    }

    /**
     * Navigate to Settings tab
     */
    public TabsAppSettingsPage navigateToSettingsPage() {
        click(settingsButtonLocator);
        return new TabsAppSettingsPage(driver);
    }
}
