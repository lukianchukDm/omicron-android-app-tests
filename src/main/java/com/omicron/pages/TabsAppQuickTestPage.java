package com.omicron.pages;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class TabsAppQuickTestPage extends BasePageObject {

    private By parametersInputsLocator = By.xpath("//*[@class='android.widget.EditText']");
    private By togglesLocator = By.xpath("//*[@class='android.widget.ImageView']");
    private By startTestButtonLocator = By.xpath("//*[@text='Start Test']");
    private By spinnerLocator = By.xpath("//*[@text='Stop']/preceding-sibling::android.widget.ImageView");
    private By stopButtonTitleLocator = By.xpath("//*[@text='Stop']");
    private By stopButtonLocator = By.xpath("//*[@text='Stop']//parent::*//parent::*");

    public TabsAppQuickTestPage(AndroidDriver<MobileElement> driver) {
        super(driver);
    }

    public String startQuickTest() {
        click(startTestButtonLocator);
        return getText(stopButtonTitleLocator);
    }

    public TabsAppQuickTestPage setParameters(List<String> params) {
        waitForVisibilityOf(parametersInputsLocator);

        List<MobileElement> inputs = findAll(parametersInputsLocator);

        for (int i = 0; i < params.size(); i++) {
            inputs.get(i).clear();
            inputs.get(i).sendKeys(params.get(i));
        }

        return this;
    }

    public TabsAppQuickTestPage setToggles(List<Boolean> togglesState) {
        List<MobileElement> toggles = findAll(togglesLocator);
        for (int i = 4; i < togglesState.size() * 3; i += 5) {
            turnOnToggle(toggles.get(i));
        }

        for (int i = 24; i < togglesState.size() * 5; i += 5) {
            turnOffToggle(toggles.get(i));
        }

        return this;
    }

    private void turnOnToggle(MobileElement toggle) {
        int coordinatesBefore = getCoordinates(toggle);

        toggle.click();
        waitForToggle(coordinatesBefore, toggle);

        if (coordinatesBefore > getCoordinates(toggle)) {
            toggle.click();
        }
    }

    private void turnOffToggle(MobileElement toggle) {
        int coordinatesBefore = getCoordinates(toggle);

        toggle.click();
        waitForToggle(coordinatesBefore, toggle);

        if (coordinatesBefore < getCoordinates(toggle)) {
            toggle.click();
        }
    }

    public boolean isSpinnerPresent() {
        return find(spinnerLocator).isDisplayed();
    }

    public boolean isStopButtonRed() throws IOException {
        Point point = find(stopButtonLocator).getCenter();
        int centerx = point.getX();
        int centerY = point.getY();

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        BufferedImage image = ImageIO.read(scrFile);
        // Getting pixel color by position x and y
        int clr = image.getRGB(centerx, centerY);
        int red = (clr & 0x00ff0000) >> 16;
        int green = (clr & 0x0000ff00) >> 8;
        int blue = clr & 0x000000ff;

        return red == 187 && green == 19 && blue == 38;
    }
}

