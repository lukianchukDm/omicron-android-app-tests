package com.omicron.pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePageObject {

    protected AndroidDriver<MobileElement> driver;

    public BasePageObject(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
    }

    /**
     * Find element using given locator
     */
    protected MobileElement find(By locator) {
        return driver.findElement(locator);
    }

    /**
     * Find all elements using given locator
     */
    protected List<MobileElement> findAll(By locator) {
        return driver.findElements(locator);
    }

    /**
     * Click on element with given locator when its visible
     */
    protected void click(By locator) {
        waitForVisibilityOf(locator);
        find(locator).click();
    }

    /**
     * Type given text into element with given locator
     */
    protected void fill(String text, By locator) {
        waitForVisibilityOf(locator);
        find(locator).sendKeys(text);
    }

    /**
     * Type given text into given element
     */
    protected void fill(String text, MobileElement element) {
        element.clear();
        element.sendKeys(text);
    }

    /**
     * Get text of an element with given locator
     */
    protected String getText(By locator) {
        waitForVisibilityOf(locator);
        return find(locator).getText();
    }

    /**
     * Get text of a given element
     */
    protected String getText(MobileElement element) {
        return element.getText();
    }

    /**
     * Get attribute of an element with given locator
     */
    protected String getAttribute(String attribute, By locator) {
        waitForVisibilityOf(locator);
        return find(locator).getAttribute(attribute);
    }

    /**
     * Get coordinates of an element
     */
    protected int getCoordinates(MobileElement element) {
        String[] coordinates = element.getLocation().toString().split("[(,]");
        return Integer.parseInt(coordinates[1]);
    }

    /**
     * Wait for specific ExpectedCondition for the given amount of time in seconds
     */
    private void waitFor(ExpectedCondition<WebElement> condition, Integer timeOutInSeconds) {
        timeOutInSeconds = timeOutInSeconds != null ? timeOutInSeconds : 10;
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(condition);
    }

    /**
     * Wait for given number of seconds for element with given locator to be visible
     * on the page
     */
    protected void waitForVisibilityOf(By locator, Integer... timeOutInSeconds) {
        int attempts = 0;
        while (attempts < 2) {
            try {
                waitFor(ExpectedConditions.visibilityOfElementLocated(locator),
                        (timeOutInSeconds.length > 0 ? timeOutInSeconds[0] : null));
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
    }

    /**
     * Wait for a toggle to change a state
     */
    protected void waitForToggle(int coordinatesBefore, MobileElement toggle) {
        int attempts = 0;
        while (attempts < 2) {
            if (coordinatesBefore == getCoordinates(toggle)) {
                attempts++;
            } else {
                break;
            }
        }
    }
}
