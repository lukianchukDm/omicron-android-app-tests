package com.omicron.pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

import java.util.List;

public class TabsAppSettingsPage extends BasePageObject {

    private By inputsLocator = By.xpath("//*[@class='android.widget.EditText']");
    private By usernameTitleLocator = By.xpath("//*[@text='User name:']");

    public TabsAppSettingsPage(AndroidDriver<MobileElement> driver) {
        super(driver);
    }

    private MobileElement getUsernameInput() {
        List<MobileElement> elements = findAll(inputsLocator);
        return elements.get(0);
    }

    public String changeUsername(String name) {
        MobileElement usernameInput = getUsernameInput();
        getUsernameInput().click();

        fill(name, usernameInput);
        click(usernameTitleLocator);

        return getText(usernameInput);
    }
}
