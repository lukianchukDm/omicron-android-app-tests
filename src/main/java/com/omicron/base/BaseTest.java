package com.omicron.base;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;

@Listeners({com.omicron.base.TestListener.class})
public class BaseTest {

    protected String testSuiteName;
    protected String testName;
    protected String testMethodName;

    protected AndroidDriver<MobileElement> driver;
    protected static AppiumDriverLocalService appiumService;
    protected String appiumUrl;

    public void startAppiumServer() {
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();

        appiumUrl = appiumService.getUrl().toString();
        System.out.println("Appium Service URL Address: " + appiumUrl);
    }

    public void initializeDriver() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "emulator-5554");
        capabilities.setCapability("platformName", "Android");

        String path = System.getProperty("user.dir") + "/src/main/resources/com.omicron.MobileStyleguide.apk";
        capabilities.setCapability(MobileCapabilityType.APP, path);
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");

        driver = new AndroidDriver<MobileElement>(new URL(appiumUrl), capabilities);
    }

    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method, ITestContext context) throws MalformedURLException {
        startAppiumServer();
        initializeDriver();

        this.testSuiteName = context.getSuite().getName();
        this.testName = context.getCurrentXmlTest().getName();
        this.testMethodName = method.getName();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        driver.quit();
        appiumService.stop();
    }
}

