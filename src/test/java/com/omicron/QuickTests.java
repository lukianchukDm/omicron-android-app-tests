package com.omicron;

import com.omicron.base.TestUtilities;
import com.omicron.pages.LandingPage;
import com.omicron.pages.TabsAppQuickTestPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class QuickTests extends TestUtilities {

    @Test
    public void startQuickTestWithParams() throws IOException {
        System.out.println("Starting 'Start the quick test with given set of parameters' Test");

        SoftAssert softAssert = new SoftAssert();
        LandingPage landingPage = new LandingPage(driver);

        TabsAppQuickTestPage quickTestPage = landingPage
                .navigateToAppWithTabsPage()
                .navigateToQuickTestPage();

        List<String> params = Arrays.asList("100", "100", "100", "200", "200", "200", "300", "300", "300",
                "100", "100", "100", "200", "200", "200", "300", "300", "300");
        List<Boolean> toggles = Arrays.asList(true, true, true, true, false, false, false, false);

        String buttonTitle = quickTestPage
                .setParameters(params)
                .setToggles(toggles)
                .startQuickTest();

        softAssert.assertEquals(buttonTitle, "Stop");
        softAssert.assertTrue(quickTestPage.isSpinnerPresent());
        softAssert.assertTrue(quickTestPage.isStopButtonRed());

        softAssert.assertAll();
    }
}
