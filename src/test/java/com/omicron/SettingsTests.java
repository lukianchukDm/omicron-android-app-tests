package com.omicron;

import com.omicron.base.TestUtilities;
import com.omicron.pages.LandingPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SettingsTests extends TestUtilities {

    @Test
    public void setUsername() {
        System.out.println("Starting 'Set the username' Test");

        String username = "Test Name";

        LandingPage landingPage = new LandingPage(driver);

        String actualUsername = landingPage
                .navigateToAppWithTabsPage()
                .navigateToSettingsPage()
                .changeUsername(username);

        Assert.assertEquals(actualUsername, username);
    }
}
